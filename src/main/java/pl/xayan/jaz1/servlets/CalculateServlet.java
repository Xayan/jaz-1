package pl.xayan.jaz1.servlets;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/calculate")
public class CalculateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html");
		
		boolean validated = validateForm(request);
		
		if(validated) {
			int kwota = 0, raty = 0, oprocentowanie = 0, oplata = 0;
			String rodzaj = "";
			
			try {
				kwota = Integer.parseInt(request.getParameter("kwota"));
				raty = Integer.parseInt(request.getParameter("raty"));
				oprocentowanie = Integer.parseInt(request.getParameter("oprocentowanie"));
				oplata = Integer.parseInt(request.getParameter("oplata"));
				rodzaj = request.getParameter("rodzaj");
			} catch(NumberFormatException | NullPointerException e) {
				e.printStackTrace();
			}
			
			String raty_arr[][] = new String[raty][5];
			int pozostalo = kwota;

			for(int i = 0; i < raty; i++) {
				double rata = 0;
				
				if(rodzaj == "malejaca") {
					rata = (double) (kwota/raty) * (1.0 + (raty - (i+1) + 1) * oprocentowanie/100.0/12.0);
				}
				else {
					double q = 1.0 + (oprocentowanie/100.0/12.0);
					rata = kwota * Math.pow(q, raty) * (q - 1.0) / (Math.pow(q, raty) - 1.0);
				}
				
				
				rata = Math.round(rata*100)/100.0;
				
				raty_arr[i][0] = Integer.toString(i+1);
				raty_arr[i][1] = Integer.toString(pozostalo);
				raty_arr[i][2] = Double.toString(rata);
				raty_arr[i][3] = Integer.toString(oplata);
				raty_arr[i][4] = Double.toString(rata + oplata);
				
				pozostalo -= rata;
			}
			
			try {
				response.getWriter().write("<table border=\"1\">");
				response.getWriter().write("<thead><tr>");
				response.getWriter().write("<th>Nr raty</th>");
				response.getWriter().write("<th>Kwota kapitału</th>");
				response.getWriter().write("<th>Kwota odsetek</th>");
				response.getWriter().write("<th>Opłaty stałe</th>");
				response.getWriter().write("<th>Całkowita kwota raty</th>");
				response.getWriter().write("</tr></thead><tbody>");
				for(int i = 0; i < raty; i++) {
					response.getWriter().write("<tr>");
					response.getWriter().write("<td>" + raty_arr[i][0] + "</td>");
					response.getWriter().write("<td>" + raty_arr[i][1] + "</td>");
					response.getWriter().write("<td>" + raty_arr[i][2] + "</td>");
					response.getWriter().write("<td>" + raty_arr[i][3] + "</td>");
					response.getWriter().write("<td>" + raty_arr[i][4] + "</td>");
					response.getWriter().write("<tr>");
				}
				response.getWriter().write("</tbody></table>");
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
		else {
			try {
				response.sendRedirect("/");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private boolean validateForm(HttpServletRequest request) {
		String kwota = request.getParameter("kwota");
		String raty = request.getParameter("raty");
		String oprocentowanie = request.getParameter("oprocentowanie");
		String oplata = request.getParameter("oplata");
		String rodzaj = request.getParameter("rodzaj");
		
		if(kwota == null || kwota.length() == 0) {
			return false;
		}
		if(raty == null || raty.length() == 0) {
			return false;
		}
		if(oprocentowanie == null || oprocentowanie.length() == 0) {
			return false;
		}
		if(oplata == null || oplata.length() == 0) {
			return false;
		}
		if(rodzaj == null || rodzaj.length() == 0) {
			return false;
		}
		
		int kwota_i, raty_i, oprocentowanie_i, oplata_i;
		
		try {
			kwota_i = Integer.parseInt(kwota);
			raty_i = Integer.parseInt(raty);
			oprocentowanie_i = Integer.parseInt(oprocentowanie);
			oplata_i = Integer.parseInt(oplata);
		} catch(NumberFormatException | NullPointerException e) {
			return false;
		}
		
		if(kwota_i < 0) {
			return false;
		}
		if(raty_i < 0) {
			return false;
		}
		if(oprocentowanie_i < 0 || oprocentowanie_i > 100) {
			return false;
		}
		if(oplata_i < 0) {
			return false;
		}
		
		return true;
	}
}
